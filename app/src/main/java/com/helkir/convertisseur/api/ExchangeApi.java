package com.helkir.convertisseur.api;

import com.helkir.convertisseur.Currency;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ExchangeApi
{
    @GET ("latest")
    Call<RatesWrapper> getCurrencies();
}
