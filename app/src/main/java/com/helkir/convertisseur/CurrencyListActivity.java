package com.helkir.convertisseur;

        import androidx.appcompat.app.AppCompatActivity;
        import androidx.recyclerview.widget.LinearLayoutManager;
        import androidx.recyclerview.widget.RecyclerView;

        import android.os.Bundle;
        import android.util.Log;

        import com.helkir.convertisseur.api.ExchangeApi;
        import com.helkir.convertisseur.api.RatesWrapper;

        import java.util.ArrayList;
        import java.util.List;

        import retrofit2.Call;
        import retrofit2.Callback;
        import retrofit2.Response;
        import retrofit2.Retrofit;
        import retrofit2.converter.gson.GsonConverterFactory;

public class CurrencyListActivity extends AppCompatActivity
{
    private CurrencyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_list);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.exchangeratesapi.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ExchangeApi api = retrofit.create(ExchangeApi.class);

        Call<RatesWrapper> call = api.getCurrencies();
        call.enqueue(new Callback<RatesWrapper>() {
            @Override
            public void onResponse(Call<RatesWrapper> call, Response<RatesWrapper> response) {
                RatesWrapper body = response.body();
                Log.i("leTag", "onResponse: "+ body);
            }

            @Override
            public void onFailure(Call<RatesWrapper> call, Throwable t) {
                Log.e("leTag", "onFailure: ", t);
            }
        });


        List<Currency> currencies = new ArrayList<>();
        currencies.add(new Currency(R.drawable.flagkorea, 1283.98f, "W"));
        currencies.add(new Currency(R.drawable.france, 1f, "€"));
        currencies.add(new Currency(R.drawable.flagkorea, 1283.98f, "W"));
        currencies.add(new Currency(R.drawable.france, 1f, "€"));
        currencies.add(new Currency(R.drawable.russia, 0.015f, "R"));
        currencies.add(new Currency(R.drawable.flagkorea, 1283.98f, "W"));
        currencies.add(new Currency(R.drawable.france, 1f, "€"));
        currencies.add(new Currency(R.drawable.russia, 0.015f, "R"));
        currencies.add(new Currency(R.drawable.flagkorea, 1283.98f, "W"));
        currencies.add(new Currency(R.drawable.france, 1f, "€"));
        currencies.add(new Currency(R.drawable.russia, 0.015f, "R"));
        currencies.add(new Currency(R.drawable.flagkorea, 1283.98f, "W"));
        currencies.add(new Currency(R.drawable.france, 1f, "€"));
        currencies.add(new Currency(R.drawable.russia, 0.015f, "R"));

        adapter = new CurrencyAdapter(currencies);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

}
