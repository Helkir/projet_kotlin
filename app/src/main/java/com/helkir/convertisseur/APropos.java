package com.helkir.convertisseur;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class APropos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apropos);
        final TextView sousTxt = findViewById(R.id.sousText);
        final TextView userName = findViewById(R.id.userName);
        final Button finishBtn = findViewById(R.id.finishBtn);
        final EditText textFinish = findViewById(R.id.textFinish);

        Intent srcIntent = getIntent();
        float sousous = srcIntent.getFloatExtra("sous", 0f );
        User userino = srcIntent.getParcelableExtra("leUser");
        sousTxt.setText(String.valueOf(sousous));
        userName.setText(userino.name);



        finishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("freestyle", textFinish.getText().toString());
                setResult(Activity.RESULT_OK, intent);
                finish(); // arrête l'Activity courante
            }
        });

    }
}
