package com.helkir.convertisseur;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("Init","coucou");

        final Button convertBtn = findViewById(R.id.convertBtn);
        final EditText inputMoney = findViewById(R.id.inputMoney);
        final TextView outputMoney = findViewById(R.id.textWon);
        final Button aproposBtn = findViewById(R.id.aProposBtn);

        convertBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("MainActivty", "coucouc");
                float inputEuro = Float.parseFloat(inputMoney.getText().toString());
                String outputWon = String.valueOf(convertWon(inputEuro));
                outputMoney.setText( outputWon );
            }
        });

        aproposBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, APropos.class);
                intent.putExtra("sous" , 3404.04f);
                User monUser = new User("Bob",20);
                intent.putExtra("leUser", monUser);
                startActivityForResult(intent,1);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( resultCode == Activity.RESULT_OK)
        {
            final TextView outputMoney = findViewById(R.id.textWon);
            outputMoney.setText( data.getStringExtra("freestyle") );
        }

    }

    private float convertWon(float value)
    {
        return value * 1283.98f; // euro to won

    }


}
