package com.helkir.convertisseur;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable
{
    String name;
    int age;


    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }


    protected User(Parcel in){
        name = in.readString();
        age = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(age);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[0];
        }
    };
}
